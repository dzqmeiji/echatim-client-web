var express = require('express');
// loading config from .env
require('dotenv').config();

var app = express();

app.use('/', express.static(__dirname + '/src/embed'));

// socket.io, http 需要跨域访问
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    next();
});

var port = process.env.EMBED_SERVER_PORT || 8080;

app.listen(port, () => {
    console.log(`start embed server at: http://localhost:${port}`);
});
