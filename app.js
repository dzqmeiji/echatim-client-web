var express = require('express');
// loading config from .env
require('dotenv').config();

var app = express();

app.use('/', express.static(__dirname + '/src'));
app.use('/assets', express.static(__dirname + '/src/assets'));
app.use('/static', express.static(__dirname + '/static'));
app.use('/dist', express.static(__dirname + '/dist'));

// socket.io, http 需要跨域访问
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    next();
});

var port = process.env.SERVER_PORT || 80;

app.listen(port, () => {
    console.log(`start server at: http://localhost:${port}`);
});
