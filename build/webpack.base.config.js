/*
 * @功能描述:
 * @作者: 高云蛟
 * @Date: 2019-08-15 22:24:12
 */
const HtmlWebpackPlugin = require('html-webpack-plugin')
const path = require('path');

module.exports = {
    entry: {
        // 'app': './src/index.ts'
        'app': './src/empty.ts'
    },
    output: {
        filename: '[name].[chunkhash:8].js',
    },
    resolve: {
        extensions: ['.js', '.ts', '.tsx']
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/i,
                use: [{
                    loader: 'ts-loader',
                    options: {
                        // configFile: path.resolve(__dirname, './tslint.json'),
                        configFile: path.resolve(__dirname, '../tslint.json')
                    },
                }],
                exclude: /node_modules/
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            // template: './src/index.html'
            template: './src/app.html'
            // template: './src/vue-chat.html'
            // template: './src/vue-old.html'
        })
    ],
    optimization: { // 拆包
        splitChunks: {
            chunks: 'all'
        }
    }
}
