const path = require('path');
// const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
    resolve: {
        extensions: ['.js', '.ts', '.json'],
    },
    devtool: 'source-map',// 打包出的js文件是否生成map文件（方便浏览器调试）
    mode: 'production',
    entry: {
        'echatim-sdk': './src/echatim.ts',
    },
    output: {
        filename: '[name].js',// 生成的fiename需要与package.json中的main一致
        path: path.resolve(__dirname, '../static/dist'),
        libraryTarget: 'window',
        library: "imsdk"
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/i,
                use: [{
                    loader: 'ts-loader',
                    options: {
                        // configFile: path.resolve(__dirname, './tslint.json'),
                        configFile: path.resolve(__dirname, '../tslint.json')
                    },
                }],
                exclude: /node_modules/
            }
        ],
    },
    plugins: [],
};
