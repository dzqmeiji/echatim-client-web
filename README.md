### PC Web客户端说明

官网: http://www.echat.work

文档: http://47.115.54.220:57070

yarn install # 安装依赖

yarn start  # 启动项目

使用自己搭建的社区版服务器时请更改以下main.js内的配置
```javascript
    sdkConfig.host = 'localhost';
    sdkConfig.httpPort = 8082;
    sdkConfig.socketPort = 9092;
    sdkConfig.key = 'TSDKTEST00001'; // 若提示SDK_KEY已失效, 修改echatim数据库表sdk_app对应数据的expire_date的字段为今天之后的时间
```


访问: http://localhost:8082


##### 嵌入页面的方式启动
``bash
# 开两个窗口, 分别执行
yarn start
yarn embed
``
访问:http://localhost:8080


##### uniapp 客户端(社交版)

客户端:

![1_2](https://gitee.com/dzqmeiji/echatim-resource/raw/master/pics/readme/echat_social_1_2.gif)

[详细请点击](http://doc.echat.work:58083/1.1%20%E4%BA%A7%E5%93%81%E4%BB%8B%E7%BB%8D/1.1.1%20%E5%95%86%E7%94%A8%E7%A4%BE%E4%BA%A4%E7%89%88/)


#### 其它:
##### 推荐购买阿里云服务器测试
安装完nginx环境后，将项目放置在html目录即可完成部署。

[详细请点击](https://www.aliyun.com/daily-act/ecs/activity_selection?userCode=bbibyrq9)

[![阿里云服务器优惠](https://gitee.com/dzqmeiji/echatim-resource/raw/master/pics/adv/aliyun_adv.png)](https://www.aliyun.com/daily-act/ecs/activity_selection?userCode=bbibyrq9)

##### 技术支持

有技术问题请加群: 471688937


