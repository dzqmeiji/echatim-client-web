var Beans = (function () {

    function bean(json){
        return JSON.parse(json);
    }

    function to(target){
        return JSON.parse(JSON.stringify(target));
    }

    function copy(target){
        return Beans.to(target);
    }

    function replace(target, replaceJSON){
        var r = Beans.to(target);
        for(var v in replaceJSON){
            r[v] = replaceJSON[v];
        }
        return r;
    }

    function json(target){
        return JSON.stringify(target)
    }

    function  strEmpty(s){
        return !!s;
    }

    function  strNotEmpty(s){
        return !s;
    }


    return {
        bean:bean,
        to:to,
        json:json,
        copy: copy,
        replace: replace,
        strEmpty: strEmpty,
        strNotEmpty: strNotEmpty,
    }
})();


var fileUtils = (function () {
    function bindImage(domId, maxFileSize, onUploaded) {
        im.newFileClient().init({
            domId:domId,
            maxFileSize: maxFileSize,// 文件大小上限. 100mb
            type:'IMAGE',
            allowSuffix:['jpeg','jpg','png']
        },{
            beforeUploadCallback:function (fileInfo) {
                return fileInfo;
            },
            progressCallback:function (fileInfo) {
            },
            uploadedCallback:function (fileInfo) {
                if(onUploaded){
                    onUploaded(fileInfo);
                }
            },
            errorCallback:function (fileInfo, errInfo) {
                if(window.app){
                    window.app.showToast(errInfo.msg);
                }
            },
        });
    }

    function bindFile(domId, maxFileSize, onProgress, onUploaded) {
        im.newFileClient().init({
            domId:domId,
            maxFileSize: maxFileSize,// 文件大小上限. 100mb
            type:'FILE',
            allowSuffix:[]
        },{
            beforeUploadCallback:function (fileInfo) {
                return fileInfo;
            },
            progressCallback:function (fileInfo) {
                if(onProgress){
                    onProgress(fileInfo);
                }
            },
            uploadedCallback:function (fileInfo) {
                if(onUploaded){
                    onUploaded(fileInfo);
                }
            },
            errorCallback:function (fileInfo, errInfo) {
                if(window.app){
                    window.app.showToast(errInfo.msg);
                }
            },
        });
    }

    return {
        bindFile:function (domId, onProgress, onUploaded) {
            bindFile(domId, 100, onProgress, onUploaded);
        },
        bindAvatar:function (domId, onUploaded) {
            bindImage(domId, 1, onUploaded);
        },
        bindImage:function (domId, onUploaded) {
            bindImage(domId, 10, onUploaded);
        }
    }
})();
